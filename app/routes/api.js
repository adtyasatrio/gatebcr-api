module.exports = (app) => {
  const cards = require("../controllers/card.controller.js");
  const logs = require("../controllers/log.controller.js");

  const router = require("express").Router();

  // Cards routes
  // Create a new Card
  router.post("/cards", cards.create);

  // Retrieve all Cards
  router.get("/cards", cards.findAll);

  // Retrieve all blocked Cards
  router.get("/cards/blocked", cards.findAllBlocked);

  // Retrieve a single Card with card_uid
  router.get("/cards/:card_uid", cards.findOne);

  // Update a Card with card_uid
  router.put("/cards/:card_uid", cards.update);

  // Delete a Card with card_uid
  router.delete("/cards/:card_uid", cards.delete);

  // Delete all Cards
  router.delete("/cards", cards.deleteAll);

  // Logs routes
  // Create a new Log
  router.post("/logs", logs.create);

  // Retrieve all Logs
  router.get("/logs", logs.findAll);

  // Retrieve a single Log with card_uid
  router.get("/logs/:card_uid", logs.findOne);

  // Update a Log with card_uid
  router.put("/logs/:card_uid", logs.update);

  // Delete a Log with card_uid
  router.delete("/logs/:card_uid", logs.delete);

  // Delete all Logs
  router.delete("/logs", logs.deleteAll);

  app.use("/api", router);
};
