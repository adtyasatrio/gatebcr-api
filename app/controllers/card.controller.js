const db = require("../models");
const Card = db.cards; // Assuming your model is named 'cards'
const Op = db.Sequelize.Op;

// const getPagination = (page, size) => {
//   const limit = size ? +size : 25;
//   const offset = page ? page * limit : 0;

//   return { limit, offset };
// };

const getPagination = (page, size) => {
  const limit = size ? +size : 25;
  const offset = page ? (page - 1) * limit : 0;

  return { limit, offset };
};

const getPagingData = (data, page, limit) => {
  const { count: totalItems, rows: cards } = data;
  const currentPage = page ? +page : 1;
  const totalPages = Math.ceil(totalItems / limit);

  return { totalItems, cards, totalPages, currentPage };
};

// Create and Save a new Card
exports.create = (req, res) => {
  // Validate request
  if (!req.body.card_uid || !req.body.name) {
    res.status(400).send({
      message: "Card UID and Name are required fields.",
    });
    return;
  }

  // Create a Card
  const card = {
    card_uid: req.body.card_uid,
    name: req.body.name,
    address: req.body.address || null,
    blocked: req.body.blocked || false,
  };

  // Save Card in the database
  Card.create(card)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the Card.",
      });
    });
};

// Retrieve all Cards from the database.
exports.findAll = (req, res) => {
  const { page, size, card_uid, blocked } = req.query;
  
  // Set up the condition based on card_uid and blocked parameters
  let condition = {};
  
  if (card_uid) {
    condition.card_uid = { [Op.like]: `%${card_uid}%` };
  }

  if (blocked !== undefined) {
    condition.blocked = blocked.toLowerCase() === 'true';
  }

  const { limit, offset } = getPagination(page, size);

  Card.findAndCountAll({ where: condition, limit, offset })
    .then((data) => {
      const response = getPagingData(data, page, limit);
      res.send(response);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving cards.",
      });
    });
};

// Retrieve all Cards from the database based on name and blocked status
// exports.findAll = (req, res) => {
//   const { page, size, name, blocked } = req.query;
//   var condition = {
//     name: name ? { [Op.like]: `%${name}%` } : null,
//     blocked: blocked ? { [Op.eq]: blocked } : 0,
//   };

//   const { limit, offset } = getPagination(page, size);

//   Card.findAndCountAll({ where: condition, limit, offset })
//     .then(data => {
//       const response = getPagingData(data, page, limit);
//       res.status(202).send(response);
//     })
//     .catch(err => {
//       res.status(500).send({
//         message:
//           err.message || "Some error occurred while retrieving cards."
//       });
//     });
// };

// Find a single Card with an id
// exports.findOne = (req, res) => {
//   const card_uid = req.params.card_uid;

//   Card.findByPk(card_uid)
//     .then((data) => {
//       res.send(data);
//     })
//     .catch((err) => {
//       res.status(500).send({
//         message: "Error retrieving Card with card_uid=" + card_uid,
//       });
//     });
// };

exports.findOne = (req, res) => {
  const card_uid = req.params.card_uid;

  Card.findOne({
    where: { card_uid, },
  })
    .then((data) => {
      if (!data) {
        return res.status(404).json({ message: 'Card not found' });
      }

      if (data.blocked) {
        return res.status(403).json({ message: 'Card found but blocked', data });
      }

      return res.status(200).json({ message: 'Card found and not blocked', data });
    })
    .catch((err) => {
      console.error(err);
      res.status(500).json({ message: 'Internal server error' });
    });
};

// Update a Card by the card_uid in the request
exports.update = (req, res) => {
  const card_uid = req.params.card_uid;

  Card.update(req.body, {
    where: { card_uid: card_uid },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Card was updated successfully.",
        });
      } else {
        res.send({
          message: `Cannot update Card with card_uid=${card_uid}. Maybe Card was not found or req.body is empty!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating Card with card_uid=" + card_uid,
      });
    });
};

// Delete a Card with the specified id in the request
exports.delete = (req, res) => {
  const card_uid = req.params.card_uid;

  Card.destroy({
    where: { card_uid },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Card was deleted successfully!",
        });
      } else {
        res.send({
          message: `Cannot delete Card with card_uid=${card_uid}. Maybe Card was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Could not delete Card with card_uid=" + card_uid,
      });
    });
};

// Delete all Cards from the database.
exports.deleteAll = (req, res) => {
  Card.destroy({
    where: {},
    truncate: false,
  })
    .then((nums) => {
      res.send({ message: `${nums} Cards were deleted successfully!` });
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while removing all cards.",
      });
    });
};

// Find all blocked Cards
exports.findAllBlocked = (req, res) => {
  const { page, size } = req.query;
  const { limit, offset } = getPagination(page, size);

  Card.findAndCountAll({ where: { blocked: true }, limit, offset })
    .then((data) => {
      const response = getPagingData(data, page, limit);
      res.send(response);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving blocked cards.",
      });
    });
};
