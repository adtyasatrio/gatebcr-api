const db = require("../models");
const Log = db.logs;
const Op = db.Sequelize.Op;

const getPagination = (page, size) => {
  const limit = size ? +size : 25;
  const offset = page ? (page - 1) * limit : 0;

  return { limit, offset };
};

const getPagingData = (data, page, limit) => {
  const { count: totalItems, rows: logs } = data;
  const currentPage = page ? +page : 0;
  const totalPages = Math.ceil(totalItems / limit);

  return { totalItems, logs, totalPages, currentPage };
};

// Create and Save a new Log
exports.create = (req, res) => {
  // Validate request
  if (!req.body.card_uid) {
    res.status(400).send({
      message: "Card UID is a required field.",
    });
    return;
  }
 
  // Create a Log
  const log = {
    card_uid: req.body.card_uid,
    status: req.body.status || null,
    gate: req.body.gate || null,
  };

  // Save Log in the database
  Log.create(log)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the Log.",
      });
    });
};

// Retrieve all Logs from the database.
exports.findAll = (req, res) => {
  const { page, size, card_uid } = req.query;
  var condition = card_uid ? { card_uid: { [Op.eq]: card_uid } } : null;

  const { limit, offset } = getPagination(page, size);

  Log.findAndCountAll({ 
    where: condition,
    order: [['createdAt', 'DESC']],
    limit, 
    offset })
    .then((data) => {
      const response = getPagingData(data, page, limit);
      res.send(response);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving logs.",
      });
    });
};

// exports.findAll = (req, res) => {
//   const { page, size, card_uid } = req.query;
//   const { limit, offset } = getPagination(page, size);

//   Log.findAndCountAll({
//     where: card_uid ? { card_uid: { [Op.eq]: card_uid } } : null,
//     include: ["cards"],
//     limit,
//     offset,
//   })
//     .then((data) => {
//       const response = getPagingData(data, page, limit);
//       res.send(response);
//     })
//     .catch((err) => {
//       res.status(500).send({
//         message: err.message || "Some error occurred while retrieving logs.",
//       });
//     });
// };

// Find a single Log with card_uid
exports.findOne = (req, res) => {
  const card_uid = req.params.card_uid;

  Log.findAndCountAll({
    where: { card_uid },
  })
    .then((data) => {
      if (!data) {
        return res.status(404).json({ message: "Log not found" });
      }

      return res.status(200).json({ message: "Log found", data });
    })
    .catch((err) => {
      console.error(err);
      res.status(500).json({ message: "Internal server error" });
    });
};

// Update a Log by the card_uid in the request
exports.update = (req, res) => {
  const card_uid = req.params.card_uid;

  Log.update(req.body, {
    where: { card_uid },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Log was updated successfully.",
        });
      } else {
        res.send({
          message: `Cannot update Log with card_uid=${card_uid}. Maybe Log was not found or req.body is empty!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating Log with card_uid=" + card_uid,
      });
    });
};

// Delete a Log with the specified card_uid in the request
exports.delete = (req, res) => {
  const card_uid = req.params.card_uid;

  Log.destroy({
    where: { card_uid },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Log was deleted successfully!",
        });
      } else {
        res.send({
          message: `Cannot delete Log with card_uid=${card_uid}. Maybe Log was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Could not delete Log with card_uid=" + card_uid,
      });
    });
};

// Delete all Logs from the database.
exports.deleteAll = (req, res) => {
  Log.destroy({
    where: {},
    truncate: false,
  })
    .then((nums) => {
      res.send({ message: `${nums} Logs were deleted successfully!` });
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while removing all logs.",
      });
    });
};
