// card.model.js
module.exports = (sequelize, Sequelize) => {
  const Card = sequelize.define("card", {
    card_uid: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    address: {
      type: Sequelize.STRING,
    },
    blocked: {
      type: Sequelize.BOOLEAN,
      defaultValue: 0,
    },
  });

  return Card;
};
