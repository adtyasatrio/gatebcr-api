// log.model.js
module.exports = (sequelize, Sequelize) => {
  const Log = sequelize.define("log", {
    card_uid: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    status: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    gate: {
      type: Sequelize.INTEGER,
      allowNull: true
    }
  });

  return Log;
};
