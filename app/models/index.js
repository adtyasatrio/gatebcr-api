const dbConfig = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  },
  timezone: dbConfig.timezone,
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.cards = require('./card.model')(sequelize, Sequelize);
db.logs = require('./log.model')(sequelize, Sequelize);

// db.logs.hasMany(db.cards, { as: "cards" });
// db.cards.belongsTo(db.logs, {
//   foreignKey: "card_uid",
//   as: "log",
// });

// db.logs.hasMany(db.cards, { as: "cards", foreignKey: "card_uid" });
// db.cards.belongsTo(db.logs, { foreignKey: "card_uid", as: "log" });

module.exports = db;